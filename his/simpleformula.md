# 协定方目录功能 (v.0.9.1) #

协定方目录是一个介面模块，提供中医师看诊时可以查询中药协定方，也可以用关键词搜寻，当医师选定要开的协定方时，按下「套用此方」，模块会将协定方信息以 callback function 的方式传送至主页面，提供服务供应商主页面开方使用。镶入介面如下：

![协定方目录介面](asset/simpleformula.png)

## 版本变更 ##

### v.0.9.1 ###

* 修正范例程序 widget.js 网址错误问题
* 协定方目录新增使用服务供应商 token 认证能力

### v.0.9 ###

* 加入协定方目录功能

## 接入说明 ##

接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>The Simple Formula Widget Demo</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'simpleformula',
    token: '诊所成员 token'
	};
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

1. 需要引入 widget.js 的 javascript library "https://module.tudihis.cn/widget/widget.js"
2. 准备好 config 资料
	* width, height: 单位是 px，为模块镶入时设置的大小
	* domain: 请传入**镶入模块的主页面域名或网址**，模块要传送信息给主页面，必须得先指定域名网址，主页面才能顺利接收到信息
	* callback: 当医生按下「套用此方」，模块会将协定方信息传出至 callback function，注意， domain 必须设对，这边才能接得到数据
	* element: 指定模块镶入的 HTML element
	* type: 请填入 'simpleformula'
	* token: 请传入**诊所成员 token** 或 **服务供应商 token**
3. 使用 cn.tudihis.widget.init(config) 就可以把模块镶入了
4. 若因为内网 nginx 转址等需求，造成连入的模块网址不是原本的 https://module.tuditcm.com ，则需要修改两个地方：
	* javascript library 引入的网址
	* 需要使用 cn.tudihis.widget.setUrl 指定新的网址

callback function 传出数据格式 JsonSimpleFormula：

_JsonSimpleFormula_

* Integer id: 协定方编号
* String name: 协定方名称
* String pinyin: 拼音码
* String source: 出处
* String func: 功效
* String symptom: 适应证
* String formulaUsage: 用法
* Integer categoryId: 所属分类编号
* JsonSimpleFormulaHerb[] herbs: 组成药

_JsonSimpleFormulaHerb_

* String name: 中药名称
* Integer dose: 剂量
* String doseUnit: 剂量单位
