# Tudi 模块系统开发手册 #

## 说明 ##

Tudi 模块系统提供医疗信息服务供应商接入模块功能，提供医疗服务加值功能，模块以 API 与 介面模块两种模式交互使用：

* API：无介面功能，使用服务供应商专用 token 作为认证，就可以调用 http API，达到交易目的，例如外配处方就属于 API 功能
* 介面模块：介面模块包括 HTML 介面，绑入方式是使用 javascript，调用 Tudi javascript library 传入相应参数并指定介面绑入的 HTML Element，javascript library 就会自动把介面镶在指定的 HTML Element 上，并且把使用者在介面上操作的关键资料以 callback function 的方式回传至主页面。

## 基础管理功能 ##

服务供应商会获得一组账号密码，可以用此账号密码取得**服务供应商 token**，并建立服务商所服务的诊所列表与成员，当诊所成员在系统上登入，可以替此诊所成员签入 Tudi 模块系统，并取得**诊所成员 token**，后续诊所服务，有些需要以**诊所成员 token**作为认证，账号登入与诊所管理，请参考 [基础管理功能](general.md)。

## 外配处方功能 ##

请参考 [外配处方功能](outmedicine.md)

## 协定方目录功能 ##

请参考 [协定方目录功能](simpleformula.md)

## 徒帝严选商城功能 ##

请参考 [徒帝严选商城功能](hisstore.md)

## 经典方功能 ##

请参考 [经典方功能](kbs.md)

## 经典方目录功能 ##

请参考 [经典方目录功能](classicformula.md)

## 单次处方点评功能 ##

请参考 [单次处方点评功能](review-single.md)

## 整批处方点评功能 ##

请参考 [整批处方点评功能](review-batch.md)

## 方剂推荐功能 ##

请参考 [方剂推荐功能](recommend-formula.md)