# 协定方目录功能 (v.0.9) #

经典方字典是一个介面模块，提供中医师看诊时可以查询中药经典方，也可以用关键词搜寻，当医师选定要开的经典方时，按下「套用此方」、「套用加减味」，模块会将经典方与加减味信息以 callback function 的方式传送至主页面，提供服务供应商主页面开方使用。镶入介面如下：

![协定方目录介面](asset/classicformula.png)

## 版本变更 ##

### v.0.9 ###

* 加入经典方字典功能

## 接入说明 ##

接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>The Classic Formula Widget Demo</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'classicformula',
    token: '诊所成员 token'
	};
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

1. 需要引入 widget.js 的 javascript library "https://module.tudihis.cn/widget/widget.js"
2. 准备好 config 资料
	* width, height: 单位是 px，为模块镶入时设置的大小
	* domain: 请传入**镶入模块的主页面域名或网址**，模块要传送信息给主页面，必须得先指定域名网址，主页面才能顺利接收到信息
	* callback: 当医生按下「套用此方」，模块会将协定方信息传出至 callback function，注意， domain 必须设对，这边才能接得到数据
	* element: 指定模块镶入的 HTML element
	* type: 请填入 'classicformula'
	* token: 请传入**诊所成员 token** 或 **服务供应商 token**
3. 使用 cn.tudihis.widget.init(config) 就可以把模块镶入了
4. 若因为内网 nginx 转址等需求，造成连入的模块网址不是原本的 https://module.tuditcm.com ，则需要修改两个地方：
	* javascript library 引入的网址
	* 需要使用 cn.tudihis.widget.setUrl 指定新的网址

callback function 传出数据分为两种形式：

* {type: 'formula', value: JsonClassicFormula}：这代表医生套用经典方
* {type: 'mod', value: JsonClassicFormulaMod}：这代表医生套用经典方的加减味

_JsonClassicFormula_

* Long id: 方剂编号
* String name: 名称
* JsonItem[] categories: 分类
* String source: 出处
* JsonItem[] symptoms: 主治症状
* JsonItem[] patterns: 主治证型
* JsonItem[] treatments: 相关治则
* JsonItem[] westernDiagnoses: 西医病名
* JsonItem usage: 用法
* JsonClassicFormulaHerb[] herbs: 用药
* JsonClassicFormulaMod[] mods: 加减味
* String remark: 备注

_JsonItem_

* Long id: 编号
* String name: 名称

_JsonClassicFormulaHerb_

* Long id: 中药编号
* String name: 中药名称
* BigDecimal dose: 用量
* String doseUnit: 单位

_JsonClassicFormulaMod_

* Long id: 加减味编号
* String source: 来源出处
* JsonClassicFormulaModHerb[] herbsAddition: 加味
* JsonItem[] herbsRemoval: 减味
* JsonItem[] symptoms: 主治症状
* JsonItem[] patterns: 主治证型

_JsonClassicFormulaModHerb_

* Long id: 药品编号
* String name: 药品名称
* BigDecimal minCount: 最小用量
* BigDecimal maxCount: 最大用量
* String unit: 单位
