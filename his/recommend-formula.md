# 方剂推荐功能 (v.0.9.4) #

## 版本变更 ##

### v.0.9.4 ###

* recommendFormulas, recommendFormulasByPattern 回传方剂组成要改传 JsonExternalPrescriptionHerb，比之前版本多家 insId (医保编码)

### v.0.9.3 ###

* recommendPatterns 传出值 JsonExternalRcmPatternResponse 新增栏位 majorSymptoms
* recommendFormulas 传入值 JsonExternalRcmFormulaRequirement 新增栏位 majorSymptoms

### v.0.9.2 ###

* 加入 API recommendFormulasByPattern

### v.0.9.1 ###

* JsonExternalRcmFormula 加入栏位 requireSymptoms

### v.0.9 ###

* 加入方剂推荐功能

## 操作流程 ##

医生开方调用方剂推荐流程：

* Step 1: 医生在开方介面已先输入病人的性别、生日、就诊日期、主诉、现病史
* Step 2: 调用 recommendPatterns 取得证型推荐列表
* Step 3: 将证型推荐列表列出让医生参考，医生可选择较可能的证型推荐方剂，此处可多选
* Step 4: 调用 recommendFormulas 取得方剂推荐列表

## API ##

调用 API 网址为

	https://module.tudihis.cn/cnmodule/kbs/[operation]

若需要权限验证，则需要在 http header 加入

	Authorization = [token]
	
Input 参数使用 post 传送

---

### recommendPatterns (推荐证型) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalRcmPatternRequirement: 证型推荐需求
	
output:

	JsonResult<JsonExternalRcmPatternResponse>: 证型推荐结果
	
_JsonExternalRcmPatternRequirement_

* String chiefComplaint: 主诉
* String currentHistory: 现病史
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* LocalDate birthday: 生日
* LocalDate recordDate: 就诊日期

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

_JsonExternalRcmPatternResponse_

* JsonItem[] symptoms: 辩认出的症状
* JsonItem[] majorSymptoms: 辩认出的主诉症状
* JsonExternalRcmPattern[] patterns: 推荐证型

_JsonExternalRcmPattern_

* Long patternId: 证型编号
* String name: 证型名称
* Integer score: 证型分数，0 - 100 分
* JsonItem[] symptoms: 证型主要症状
* JsonItem[] matchedSymptoms: 病人符合的症状
* JsonItem[] requireSymptoms: 尚需要确定症狀，此栏位若有内容，代表这个推荐的证型不确定，建议医生再多诊断尚需确认的症状

---

### recommendFormulas (推荐方剂) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalRcmFormulaRequirement: 方剂推荐需求
	
output:

	JsonResult<JsonExternalRcmFormula[]>: 方剂推荐结果

_JsonExternalRcmFormulaRequirement_

* JsonItem[] symptoms: 辩认出的症状，沿用证型推荐的结果
* JsonItem[] majorSymptoms: 辩认出的主诉症状，沿用证型推荐的结果
* JsonExternalRcmPattern[] patterns: 医生选择的证型
	
_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

_JsonExternalRcmFormula_

* Long formulaId: 方剂编号
* String formulaName: 方剂名称
* JsonExternalPrescriptionHerb[] herbs: 组成药
* JsonItem[] symptoms: 方剂主治症状
* JsonItem pattern: 证型
* Integer score: 方剂分数，0 - 100 分
* JsonItem[] requireSymptoms: 尚需要确定症狀，此栏位若有内容，代表这个推荐的方剂不确定，建议医生再多诊断尚需确认的症状

_JsonExternalPrescriptionHerb_

* Long id: 知识库中药编号
* String name: 名称
* BigDecimal dose: 单帖剂量
* String unit: 单位
* BigDecimal price: 单价
* String insId: 医保编码

---

### recommendFormulasByPattern (推荐方剂) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalRcmFormulaByPatternRequirement: 方剂推荐需求
	
output:

	JsonResult<JsonExternalRcmFormula[]>: 方剂推荐结果

_JsonExternalRcmFormulaByPatternRequirement_

* String chiefComplaint: 主诉
* String currentHistory: 现病史
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* LocalDate birthday: 生日
* LocalDate recordDate: 就诊日期
* String patternName: 证型名称
	
_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

_JsonExternalRcmFormula_

* Long formulaId: 方剂编号
* String formulaName: 方剂名称
* JsonExternalPrescriptionHerb[] herbs: 组成药
* JsonItem[] symptoms: 方剂主治症状
* JsonItem pattern: 证型
* Integer score: 方剂分数，0 - 100 分
* JsonItem[] requireSymptoms: 尚需要确定症狀，此栏位若有内容，代表这个推荐的方剂不确定，建议医生再多诊断尚需确认的症状

_JsonExternalPrescriptionHerb_

* Long id: 知识库中药编号
* String name: 名称
* BigDecimal dose: 单帖剂量
* String unit: 单位
* BigDecimal price: 单价
* String insId: 医保编码