# 整批处方点评功能 (v.0.9.11) #

整批处方点评提供 HIS 有能力将诊所、医生、病人与就诊记录信息送至中医大脑，产生整批处方点评，点评结果可以用诊所、医生、单次就诊记录等不同层次的模块呈现。

## 版本变更 ##

### v.0.9.11 ###

* 加入 getStatisticDashboard

### v.0.9.10 ###

* fetchReviewSimpleRecords 回传值加入 oriClinicId

### v.0.9.9 ###

* API fetchReviewSimpleRecords 传出值加入参数 score，改以 score 决定 warningLevel
* reviewRuleType 规则编号加入特殊超量项目

### v.0.9.8 ###

* API fetchReviewSimpleRecords 加入参数 warningLevel

### v.0.9.7 ###

* API addReviewClinics, addReviewPatients, addReviewDoctors, addReviewRecords 回传值加入 errors 栏位，可以查看错误编号与信息

### v.0.9.6 ###

* API addReviewClinics 回传值改为 JsonResult<JsonExternalInputResult>，包含错误编号与信息
* API addReviewPatients 回传值改为 JsonResult<JsonExternalInputResult>，包含错误编号与信息
* API addReviewDoctors 回传值改为 JsonResult<JsonExternalInputResult>，包含错误编号与信息
* API addReviewRecords 回传值改为 JsonResult<JsonExternalInputRecordResult>，包含错误编号与信息，回传正常信息将 successIds 改为 successRecordIds、successPrescriptionIds，回传成功的就诊记录号与处方编号
* API batchReview 回传值改为 JsonResult<JsonExternalReviewBatchDashboard>，回传错误信息与编号
* 加入 fetchDoctorReviewStatistic API
* 加入 fetchReviewSimpleRecords API

### v.0.9.5 ###

* API addReviewClinics 回传值 JsonExternalInputResult 加入 successId。
* API addReviewPatients 回传值 JsonExternalInputResult 加入 successId。
* API addReviewDoctors 回传值 JsonExternalInputResult 加入 successId。
* API addReviewRecords 回传值 JsonExternalInputResult 加入 successId。
* 整批点评进度模块更新介面，改为每次 batchReview 都会产生一张进度表，进度表加上实际数量。
* 介面模块 review-batch-clinic、review-batch-doctors、review-batch-patients、review-batch-record 在接到尚未上传的编号时，显示友善提示信息。

### v.0.9.4 ###

* 加入介面模块附图
* 修正 javascript code format

### v.0.9.3 ###

* API addReviewClinics 回传值改成 JsonExternalInputResult。
* API addReviewPatients 回传值改成 JsonExternalInputResult。
* API addReviewDoctors 回传值改成 JsonExternalInputResult。
* API addReviewRecords 回传值改成 JsonExternalInputResult。
* 新增 JsonExternalInputRecord、JsonExpRecordDiagnosis 长度限制说明。

### v.0.9.2 ###

* API addReviewPatients 对于传入不在系统中的 oriClinicId，改为直接建立此 oriClinicId 的预设诊所
* API addReviewDoctors 对于传入不在系统中的 oriClinicId，改为直接建立此 oriClinicId 的预设诊所
* API addReviewRecords 对于传入不在系统中的 oriClinicId、oriPatientId、oriDoctorId，改为直接建立预设诊所、预设病人、预设医生
* JsonExternalInputRecord 新增栏位 prescriptions、oriClinicId
* JsonExternalInputRecord 删除栏位 prescription、formulaCount
* 新增 JsonExternalInputPrescription

### v.0.9.1 ###

* 加入整批点评 API，整批点评计算进度表、诊所点评、医生点评、病人病历点评、单张处方点评介面模块

### v.0.9 ###

* 加入诊所、医生、病人、就诊记录数据上传功能

## 操作流程 ##

* Step 1: HIS 可以定期将欲使用整批处方点评的诊所、医生、病人、就诊记录以 API addReviewClinics、addReviewDoctors、addReviewPatients、addReviewRecords 分别上传
* Step 2: 使用 API batchReview 启动整批点评。
* Step 3: 整批点评处理进度可以用介面模块 review-batch-analysis 查看
* Step 4: 处理完毕，可以让使用者选择诊所跟点评开始结束日期，使用介面模块 review-batch-clinic 展示诊所在指定时间区间的处方点评结果
* Step 5: 也可以使用介面模块 review-batch-doctors 展示诊所中各医生在指定时间区间的处方点评结果
* Step 6: review-batch-doctors 模块可以让使用者选择医生，当在 callback 接到医生的原系统编号 oriDoctorId 之后，可以调用介面模块 review-batch-patients 展示此医生各病人处方点评结果
* Step 7: review-batch-patients 模块可以让使用者点选就诊记录，当在 callback 接到就诊记录原系统编号 oriRecordId 之后，可以调用介面模块 review-batch-record 展示此就诊记录的详细点评结果

## API ##

调用 API 网址为

	https://module.tudihis.cn/cnmodule/kbs/[operation]

若需要权限验证，则需要在 http header 加入

	Authorization = [token]
	
Input 参数使用 post 传送

---

### addReviewClinics (加入诊所信息) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputClinic[]: 诊所信息，请用 post body 传入
	
output:

	JsonResult<JsonExternalInputResult>: 新增修改几笔资料
	
_JsonExternalInputClinic_

* Long oriId: 原系统诊所编号
* String name: 原系统诊所名称

_JsonExternalInputResult_

* Integer newCount: 新增数量
* Integer updateCount: 修改数量
* Long[] successIds: 成功的原系统编号
* JsonExternalInputError[] errors: 错误列表

_JsonExternalInputError_

* Long id: 错误原系统诊所编号
* String errorMessage: 错误信息

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### addReviewPatients (加入病人信息) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputPatient[]: 病人信息，请用 post body 传入
	
output:

	JsonResult<JsonExternalInputResult>: 新增修改几笔资料

_JsonExternalInputPatient_

* Long oriId: 原系统病人编号
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* String birthday: 生日，yyyy/MM/dd
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputResult_

* Integer newCount: 新增数量
* Integer updateCount: 修改数量
* Long[] successIds: 成功的原系统编号
* JsonExternalInputError[] errors: 错误列表

_JsonExternalInputError_

* Long id: 错误原系统病人编号
* String errorMessage: 错误信息

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### addReviewDoctors (加入医生信息) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputDoctor[]: 医生信息，请用 post body 传入
	
output:

	JsonResult<JsonExternalInputResult>: 新增修改几笔资料
	
_JsonExternalInputDoctor_

* Long oriId: 原系统医生编号
* String name: 医生姓名
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputResult_

* Integer newCount: 新增数量
* Integer updateCount: 修改数量
* Long[] successIds: 成功的原系统编号
* JsonExternalInputError[] errors: 错误列表

_JsonExternalInputError_

* Long id: 错误原系统医生编号
* String errorMessage: 错误信息

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### addReviewRecords (加入就诊记录信息) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputRecord[]: 就诊记录信息，请用 post body 传入
	
output:

	JsonResult<JsonExternalInputRecordResult>: 新增修改几笔资料

_JsonExternalInputRecord_

* Long oriId: 原系统就诊记录编号
* String chiefComplaint: 主诉，长度最长 400 字
* String currentHistory: 现病史，长度最长 400 字
* String patternTreatment: 证型治则，长度最长 400 字
* JsonExpRecordDiagnosis[] diagnosis: 诊断
* JsonExternalInputPrescription[] prescriptions: 处方
* String recordTime: 就诊时间，yyyy/MM/dd HH:mm:ss
* Long oriPatientId: 原系统病人编号
* Long oriDoctorId: 原系统医生编号
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputPrescription_

* Long oriPrescriptionId: 原系统处方编号
* Integer formulaCount: 帖数
* String usage: 用法，例如口服、代茶饮，用来区分不同处方用法不同
* JsonExpPrescriptionHerb[] prescription: 处方

_JsonExpPrescriptionHerb_

* Long id: 请带入 0 即可
* String name: 中药名称
* BigDecimal dose: 单帖剂量
* String unit: 单位
* BigDecimal price: 单价

_JsonExpRecordDiagnosis_

* String insId: 诊断编码，若无编码请传空字串，长度最长 25 字
* String name: 诊断名称，长度最长 300 字

_JsonExternalInputRecordResult_

* Integer newCount: 新增数量
* Integer updateCount: 修改数量
* Long[] successRecordIds: 成功的原系统就诊记录编号
* Long[] successPrescriptionIds: 成功的原系统处方编号
* JsonExternalInputError[] errors: 错误列表

_JsonExternalInputError_

* Long id: 错误原系统就诊记录编号
* String errorMessage: 错误信息

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### batchReview (批次点评就诊记录) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	不需传入参数
	
output:

	JsonResult<JsonExternalReviewBatchDashboard>: 目前进度，不需使用这个回传值，可直接调用 review-batch-analysis 模块查看进度
	
_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### fetchDoctorReviewStatistic (取得医生点评列表) ###

所需权限：诊所成员 token 或服务供应商 token

__[in]__

	Long clinicOriId: 诊所原系统编号
	String fromDate: 开始日期，yyyy/MM/dd
	String toDate: 结束日期，yyyy/MM/dd

__[out]__

	JsonResult<JsonExpReviewStatistic[]>: 医生点评列表
	
_JsonExpRuleScore_

* Integer reviewRuleType: 规则编号
	* 1 = 记录症狀
	* 2 = 记录舌象
	* 3 = 记录脉象
	* 4 = 记录证型
	* 5 = 记录诊断病名
	* 6 = 记录诊断证型
	* 7 = 不违反十八反十九畏
	* 8 = 不违反安全用药
	* 9 = 不违反普通疾病不超过20味
	* 10 = 不违反肿瘤不超过25味
	* 11 = 不违反普通病不超过7贴/次
	* 12 = 不违反慢性病不超过14贴/次
	* 13 = 不违反普通病均贴不超过一个30元/贴
	* 14 = 不违反恶性肿瘤等医保规定病种放宽到少于50元/贴
	* 15 = 不连续使用大毒中药
	* 16 = 不对老人使用危险中药
	* 17 = 不对儿童使用危险中药
	* 18 = 不对孕妇使用危险中药
	* 19 = 症状证型符合
	* 20 = 使用经典方或药对
	* 21 = 方证符合
	* 22 = 符合常规用药量
	* 23 = 大毒饮片不超量
	* 24 = 贵细饮片不超量
	* 25 = 超过半数饮片不超量
* Integer failedRecordCount: 不及格就诊记录数
* Integer score: 平均分数

_JsonExpReviewStatistic_

* Long id: 医生编号
* Long oriId: 原系統医生編號
* String name: 医生名
* Integer recordCount: 就诊记录总量
* Integer patientCount: 病人总量
* JsonExpRuleScore[] scores: 点评规则分数列表

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### fetchReviewSimpleRecords (取得就診紀錄簡單點評) ###

所需权限：诊所成员 token 或服务供应商 token

__[in]__

	long[] recordOriIds: 就诊记录原系统编号列表，请用 post body 传入 JSON
	
__[out]__

	JsonResult<JsonExpSimpleRecord[]>: 就诊记录简单点评列表

_JsonExpSimpleRecord_

* Long id: 就诊记录编号
* Long oriId: 原系统就诊记录编号
* Long oriClinicId: 原系统医疗机构编号
* JsonExpPatientInfo patientInfo: 病人资料
* String[] diagnoses: 诊断名称
* Integer[] violationIds: 违反规则编号列表
	* 1 = 记录症狀 (无症狀)
	* 2 = 记录舌象 (无舌象)
	* 3 = 记录脉象 (无脉象)
	* 4 = 记录证型 (无证型)
	* 5 = 记录诊断病名 (无诊断病名)
	* 6 = 记录诊断证型 (无诊断证型)
	* 7 = 不违反十八反十九畏 (十八反十九畏)
	* 8 = 不违反安全用药 (危险用药)
	* 9 = 不违反普通疾病不超过20味 (药味过多)
	* 10 = 不违反肿瘤不超过25味 (药味过多)
	* 11 = 不违反普通病不超过7贴/次 (帖数过多)
	* 12 = 不违反慢性病不超过14贴/次 (帖数过多)
	* 13 = 不违反普通病均贴不超过一个30元/贴 (费用过高)
	* 14 = 不违反恶性肿瘤等医保规定病种放宽到少于50元/贴 (费用过高)
	* 15 = 不连续使用大毒中药 (危险中药)
	* 16 = 不对老人使用危险中药 (危险中药)
	* 17 = 不对儿童使用危险中药 (危险中药)
	* 18 = 不对孕妇使用危险中药 (危险中药)
	* 19 = 症状证型符合 (症证不合)
	* 20 = 使用经典方或药对 (无经典方或药对)
	* 21 = 方证符合 (方症不合)
	* 22 = 符合常规用药量 (剂量过大)
	* 23 = 大毒饮片不超量 (剂量过大)
	* 24 = 贵细饮片不超量 (剂量过大)
	* 25 = 超过半数饮片不超量 (剂量过大)
* Integet score: 分数，0 - 100 
* Integer warningLevel: 警告等级，0 = 没问题 (score >= 80), 1 = 打招乎 (score >= 60 && score < 80), 2 = 提醒 (score >= 40 && score < 60), 3 = 警告 (score >= 20 && score < 40), 4 = 禁止 (score < 20)
* LocalDateTime finishTime: 就诊记录完成时间

_JsonExpPatientInfo_

* Long patientId: 病人编号
* Long oriPatientId: 原系统病人编号
* Long recordId: 就诊记录编号
* Long oriRecordId: 原系统就诊记录编号
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* Integer age: 年纪
* Integer ageType: 年龄单位，1 = 岁, 2 = 月
* Integer role: 0 = 一般人, 1 = 老人, 2 = 小兒, 3 = 孕妇
* Integer[] specialDisease: 1 = 癌症, 2 = 慢性病
	
_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### getStatisticDashboard (取得整批点评大屏数据) ###

所需权限：诊所成员 token 或服务供应商 token
	
__[in]__

* String fromDate: 开始日期，yyyy/MM/dd
* String toDate: 结束日期，yyyy/MM/dd

__[out]__

* JsonResult<JsonExpStatisticDashboard>: 整批点评大屏数据

_JsonExpStatisticDashboard_

* JsonExpStatisticMonthlyCount[] recordNoCompleteList: 脉案记录不全处方量 (0 = 全部、1 = 症状、2 = 舌象、3 = 脉象、4 = 证型)
* JsonExpStatisticMonthlyCount[] diagnosisNoCompleteList: 诊断不全处方量 (0 = 全部、5 = 病名、6 = 中医证型)
* JsonExpStatisticMonthlyCount[] herbCountOverList: 药味使用超量处方量 (0 = 全部)
* JsonExpStatisticMonthlyCount[] formulaCountOverList: 帖数超量处方量 (0 = 全部)
* JsonExpStatisticMonthlyCount[] poisonList: 危险毒麻饮片处方量 (0 = 全部、15 = 连续使用大毒饮片、16 = 老人、17 = 小孩、18 = 孕妇)
* JsonExpStatisticMonthlyCount[] oppositeList: 违反十八反十九畏处方量 (0 = 全部)
* JsonExpStatisticMonthlyCount[] warningList: 违反方药禁忌处方量 (0 = 全部)
* JsonExpStatisticMonthlyCount[] patternNoMatchList: 症状证型不合处方量 (0 = 全部)
* JsonExpStatisticMonthlyCount[] prescriptionNoMatchList: 方证不合处方量 (0 = 全部)

_JsonExpStatisticCount_

* Integer type: 类型
* Integer count: 数量

_JsonExpStatisticMonthlyCount_

* String monthName: 年月名称，yyyy/MM
* JsonExpStatisticCount[] countList: 各类型数量

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

## 介面模块接入 ##

### 通用规则 ###

1. 需要引入 widget.js 的 javascript library "https://module.tudihis.cn/widget/widget.js"
2. 准备好 config 资料
	* width, height: 单位是 px，为模块镶入时设置的大小
	* domain: 请传入**镶入模块的主页面域名或网址**，模块要传送信息给主页面，必须得先指定域名网址，主页面才能顺利接收到信息
	* callback: 此模块无回传动作
	* element: 指定模块镶入的 HTML element
	* type: 请填入 'review-single'
	* token: 请传入**诊所成员 token** 或 **服务供应商 token**
	* kbsToken: 请传入第一步 API 回传的点评 token
3. 使用 cn.tudihis.widget.init(config) 就可以把模块镶入了
4. 若因为内网 nginx 转址等需求，造成连入的模块网址不是原本的 https://module.tuditcm.com ，则需要修改两个地方：
	* javascript library 引入的网址
	* 需要使用 cn.tudihis.widget.setUrl 指定新的网址
	
### 整批点评进度模块 (review-batch-analysis) ###

![整批点评进度介面](asset/reviewbatchanalysis.png)

接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>Review Batch Analysis</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'review-batch-analysis',
    token: '诊所成员 token'
  };
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

* config.type 请传入 review-batch-analysis

### 诊所点评模块 (review-batch-clinic) ###

![诊所点评介面](asset/reviewbatchclinic.png)

展示一家诊所整体处方质量点评结果，接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>Review Batch Clinic</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'review-batch-clinic',
    token: '诊所成员 token',
    clinicOriId: '8888',
    fromDate: '2022/01/01',
    toDate: '2022/07/31
  };
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

* config.type 请传入 review-batch-clinic
* config.clinicOriId 请传入要展示诊所的原系统编号
* config.fromDate, config.toDate 为点评的开始结束日期，格式为 yyyy/MM/dd

### 医生点评模块 (review-batch-doctors) ###

![医生点评介面](asset/reviewbatchdoctors.png)

展示一家诊所中每位医生整体处方质量点评结果，接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>Review Batch Doctors</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'review-batch-doctors',
    token: '诊所成员 token',
    clinicOriId: '8888',
    fromDate: '2022/01/01',
    toDate: '2022/07/31,
    showDoctorButton: '1'
  };
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

* config.type 请传入 review-batch-doctors
* config.clinicOriId 请传入要展示诊所的原系统编号
* config.fromDate, config.toDate 为点评的开始结束日期，格式为 yyyy/MM/dd
* config.showDoctorButton 指定是否显示按钮让使用者点选医生，若传入 '1' 则显示，'0' 则隐藏
* 若显示医生按钮，当使用者点选按钮，callback 会回传此医生的原系统编号 doctorOriId

### 医生病人点评模块 (review-batch-patients) ###

![医生病人点评介面](asset/reviewbatchpatients.png)

展示一位医生每位病人处方质量点评结果，接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>Review Batch Patients</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'review-batch-patients',
    token: '诊所成员 token',
    doctorOriId: '999',
    fromDate: '2022/01/01',
    toDate: '2022/07/31
  };
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

* config.type 请传入 review-batch-patients
* config.doctorOriId 请传入要展示医生的原系统编号
* config.fromDate, config.toDate 为点评的开始结束日期，格式为 yyyy/MM/dd
* 模块中使用者可以点选病人的单次就诊记录，点选时 callback 会回传此就诊记录的原系统编号 oriRecordId

### 就诊记录点评模块 (review-batch-record) ###

![就诊记录点评介面](asset/reviewbatchrecord.png)

展示一次就诊记录点评结果，接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>Review Batch Record</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'review-batch-record',
    token: '诊所成员 token',
    recordOriId: '666'
  };
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

* config.type 请传入 review-batch-record
* config.recordOriId 请传入要展示就诊记录的原系统编号
