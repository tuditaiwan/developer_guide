# 外配处方功能 (v.0.9.5) #

## 版本变更 ##

### v.0.9.5 ###

* fetchOutHerbs 回传值增加国家编码栏位
* matchHerbs 回传值增加国家编码，传入 service id = 0 可以提供国家编码目录名称

### v.0.9.4 ###

* sendOutOrder 加入对 id = 0 的中药以药名自动匹配功能

### v.0.9.3 ###

* 增加药名匹配机制

### v.0.9.2 ###

* 认证机制改成使用诊所成员 token

### v.0.9.1 ###

* JsonOutOrder 的 recordId 改为 String
* JsonOutOrderHerb 的参数 count 说明修改

### v.0.9 ###

* JsonOutOrder 加入 id, serviced
* 加入JsonSimpleOutOrder
* API 取得订单列表改用 getSimpleOutOrders
* 加入 getOutOrder 取得单一订单详细资料
* 加入 API 判断外配运费 (calculateDeliverFee)

### v.0.8 ###

* 加入外配处方功能

## 操作流程 ##

医生开方流程：

* Step 1: 当诊所医生进入系统，先取得预存的 Tudi 系统诊所编号
* Step 2: 准备开处方前，使用 getAvaliableService 得到本诊所可以使用的代煎厂商
* Step 3: 开方时，使用 fetchOutHerbs 取得厂商中草药目录
* Step 4: 开方完毕，使用 calculateDeliverFee 判断配送运费，若运费需要代收，则让诊所划价时要收这笔费用
* Step 5: 病人划价后，使用 sendOutOrder 送出处方
* Step 6: 若医生开错处方或病人退费，可使用 refundOrder 取消订单，但若煎药中心已经开始处理，会回踯错误不允许取消订单

查询订单流程：

* Step 1: 使用 getSimpleOutOrders 取得时间区段中所有订单
* Step 2: 使用 getOutOrder 可以查出订单详情

## API ##

调用 API 网址为

	https://module.tudihis.cn/cnmodule/outmedicine/[operation]

若需要权限验证，则需要在 http header 加入

	Authorization = [token]
	
Input 参数使用 post 传送

---

### fetchOutHerbs (取得外配药品列表) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer serviceId: 服务商编号
	
output:

	JsonOutHerb[]: 服务商供应中药列表
	
_JsonOutHerb_

* Integer id: 外配中药编号
* Integer serviceId: 服务商编号
* String name: 药品名称
* String pinyin: 拼音码
* String specification: 规格
* String origin: 产地
* String unit: 单位
* Float outPrice: 零售价
* Float cashBackRatio: 医药公司返利给诊所的比例
* String nationwideId: 国家编码
	
---

### sendOutOrder (送出外配订单) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonOutOrder: 外配中药订单，请将 json 放入 http body 中
	
output:

	Integer orderId: 订单编号
	
_JsonOutOrder_

* Integer id: 订单编号
* Integer serviceId: 煎药中心服务供货商编号
* Integer clinicId: 诊所编号
* String recordId: 就诊纪录号
* Integer patientId: 病人编号
* String patientName: 病人姓名
* Integer patientGender: 病人性别，1 = 男, 2 = 女, 3 = 其他
* Integer patientAge: 病人年龄
* String patientPhone: 病人联系电话
* String patientAddress: 病人地址
* String doctorId: 医生工号
* String doctorName: 医生姓名
* String division: 科室名称
* Integer cookType: 0 = 自煎, 1 = 代煎
* String remark: 其他煎药需求
* String usage: 中药用法，例如口服
* String frequency: 服用频率，例如每日2次，每次1包
* String[] diagnoses: 诊断
* String chiefComplaint: 主诉
* String currentHistory: 现病史
* Integer deliverType: 0 = 要送至诊所自送, 1 = 煎药中心代送至病人地址
* String deliverAddress: 送货地址
* String receiver: 收件人
* String receiverPhone: 收件人电话
* JsonOutOrderHerb[] prescription: 处方
* Integer count: 帖数
* Integer packetCount: 每帖煎几包
* BigDecimal deliverFee: 运费
* BigDecimal totalPrice: 看诊总金额，诊所会要求为药费 + 运费 + 诊所额外收费

_JsonOutOrderHerb_

* Integer id: 外配中药编号，若填 0，则系统会自动以药名匹配煎药中心品种，若无法匹配则抛出错误
* String name: 中药名称
* String unit: 单位
* Float price: 中草药一单位剂量单价
* Float count: 单帖剂量
* String cookMethod: 煎法，例如煎服、先煎、包煎

---

### getAvaliableService (取得诊所可用外配中草药服务商) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer clinicId: 诊所编号
	
output:

	JsonItem[]: 外配服务商编号、名称

_JsonItem_

* Integer id: 外配服务商编号
* Stringn name: 外配服务商名称

---

### refundOrder (取消订单) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer orderId: 订单编号
	
output:

	JsonOutOrder: 被取消的订单，若报错代表煎药中心已开始处理，拒绝取消

_JsonOutOrder_

* Integer id: 订单编号
* Integer serviceId: 煎药中心服务供货商编号
* Integer clinicId: 诊所编号
* String recordId: 就诊纪录号
* Integer patientId: 病人编号
* String patientName: 病人姓名
* Integer patientGender: 病人性别，1 = 男, 2 = 女, 3 = 其他
* Integer patientAge: 病人年龄
* String patientPhone: 病人联系电话
* String patientAddress: 病人地址
* String doctorId: 医生工号
* String doctorName: 医生姓名
* String division: 科室名称
* Integer cookType: 0 = 自煎, 1 = 代煎
* String remark: 其他煎药需求
* String usage: 中药用法，例如口服
* String frequency: 服用频率，例如每日2次，每次1包
* String[] diagnoses: 诊断
* String chiefComplaint: 主诉
* String currentHistory: 现病史
* Integer deliverType: 0 = 要送至诊所自送, 1 = 煎药中心代送至病人地址
* String deliverAddress: 送货地址
* String receiver: 收件人
* String receiverPhone: 收件人电话
* JsonOutOrderHerb[] prescription: 处方
* Integer count: 帖数
* Integer packetCount: 每帖煎几包
* BigDecimal deliverFee: 运费
* BigDecimal totalPrice: 看诊总金额，诊所会要求为药费 + 运费 + 诊所额外收费

_JsonOutOrderHerb_

* Integer id: 外配中药编号
* String name: 中药名称
* String unit: 单位
* Float price: 中草药一单位剂量单价
* Float count: 单帖剂量
* String cookMethod: 煎法，例如煎服、先煎、包煎
	
---

### getSimpleOutOrders (取得订单列表) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	String fromDate: 开始日期，yyyy/MM/dd
	String toDate: 结束日期，yyyy/MM/dd
	
output:

	JsonSimpleOutOrder []: 成立订单列表

_JsonSimpleOutOrder_

* Integer id: 订单编号
* String patientName: 病人姓名
* String doctorName: 医生姓名
* Integer clinicId: 诊所编号
* String clinicName: 诊所名称
* BigDecimal deliverFee: 运费
* BigDecimal totalPrice: 看诊总金额，诊所会要求为药费 + 运费 + 诊所额外收费
* LocalDateTime createTime: 建立时间
* Integer status: 1= 正式订单, 9 = 已取消
	
---

### getOutOrder (取得订单详细资料) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer orderId: 订单编号
	
output:

	JsonOutOrder: 指定的订单

_JsonOutOrder_

* Integer id: 订单编号
* Integer serviceId: 煎药中心服务供货商编号
* Integer clinicId: 诊所编号
* String recordId: 就诊纪录号
* Integer patientId: 病人编号
* String patientName: 病人姓名
* Integer patientGender: 病人性别，1 = 男, 2 = 女, 3 = 其他
* Integer patientAge: 病人年龄
* String patientPhone: 病人联系电话
* String patientAddress: 病人地址
* String doctorId: 医生工号
* String doctorName: 医生姓名
* String division: 科室名称
* Integer cookType: 0 = 自煎, 1 = 代煎
* String remark: 其他煎药需求
* String usage: 中药用法，例如口服
* String frequency: 服用频率，例如每日2次，每次1包
* String[] diagnoses: 诊断
* String chiefComplaint: 主诉
* String currentHistory: 现病史
* Integer deliverType: 0 = 要送至诊所自送, 1 = 煎药中心代送至病人地址
* String deliverAddress: 送货地址
* String receiver: 收件人
* String receiverPhone: 收件人电话
* JsonOutOrderHerb[] prescription: 处方
* Integer count: 帖数
* Integer packetCount: 每帖煎几包
* BigDecimal deliverFee: 运费
* BigDecimal totalPrice: 看诊总金额，诊所会要求为药费 + 运费 + 诊所额外收费

_JsonOutOrderHerb_

* Integer id: 外配中药编号
* String name: 中药名称
* String unit: 单位
* Float price: 中草药一单位剂量单价
* Float count: 单帖剂量
* String cookMethod: 煎法，例如煎服、先煎、包煎
	
---

### calculateDeliverFee (判断外配运费) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	String address: 地址
	BigDecimal herbTotalPrice: 药费总金额
	Integer medicineServiceId: 煎药中心编号

output:

	JsonOutDeliverFee: 运费与收费方式
	
_JsonOutDeliverFee_

* Integer type: 1 = 诊所代收, 2 = 货到付款
* BigDecimal price: 诊所代收金额，若是货到付款，则一律为 0

---

### matchHerbs (以药名匹配煎药中心品种) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonMatchOutHerbRequest: 中草药名称匹配请求

output:

	JsonOutHerb[]: 匹配结果，与传入名称列表照顺序一对一，若匹配不到，则回传 id = 0 的 JsonOutHerb
	
_JsonMatchOutHerbRequest_

* String[] herbNames: 待匹配名称列表
* Integer medicineServiceId: 煎药中心编号，若传入 0 则会回传国家编码标准名称

_JsonOutHerb_

* Integer id: 外配中药编号
* Integer serviceId: 服务商编号
* String name: 药品名称
* String pinyin: 拼音码
* String specification: 规格
* String origin: 产地
* String unit: 单位
* Float outPrice: 零售价
* Float cashBackRatio: 医药公司返利给诊所的比例
* String nationwideId: 国家编码