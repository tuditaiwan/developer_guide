# 经典方功能 (v.0.9.5) #

## 版本变更 ##

### v.0.9.5 ###

* getClassicFormulaList 功能加入参数 page
* 新增 API fetchClassicFormulasWithCategory

### v.0.9.4 ###

* 修改 getClassicFormulaList 回传数据格式为 

### v.0.9.3 ###

* 加入 getClassicFormulaList

### v.0.9.2 ###

* getHerbInfo 回传值改为 JsonResult<JsonExpHerbInfo>

### v.0.9.1 ###

* 加入中草药资料 getHerbInfo 功能

### v.0.9 ###

* 加入经典方搜寻处方功能

## 操作流程 ##

医生开方调用经典方流程：

* Step 1: 医生开方输入部份经典方名称或简拼
* Step 2: 调用 searchKBSFormulaNames 搜寻经典方列表，供医生选择
* Step 3: 医生选择经典方，以经典方编号调用 getKBSFormula，取得经典方内容
* Step 4: 将经典方用药带入医生处方，若需要转换为外配处方用药或国家编码，请使用 outmedicine.matchHerbs

## API ##

调用 API 网址为

	https://module.tudihis.cn/cnmodule/kbs/[operation]

若需要权限验证，则需要在 http header 加入

	Authorization = [token]
	
Input 参数使用 post 传送

---

### searchKBSFormulaNames (搜寻经典方列表) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	String keyword: 关键字，可以是部份名称或简拼
	
output:

	JsonItem[]: 经典方列表
	
_JsonItem_

* Integer id: 经典方编号
* String name: 经典方名称
	
---

### getKBSFormula (取得经典方内容) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer formulaId: 经典方编号
	
output:

	JsonKbsFormula: 经典方内容
	
_JsonClassicFormulaHerb_

* Integer id: 经典方中药编号
* String name: 中药名称
* BigDecimal dose: 剂量
* String doseUnit: 单位

_JsonClassicFormula_

* Long id: 经典方编号
* String name: 名称
* JsonItem[] categories: 分类
* String source: 出处
* JsonItem[] symptoms: 主治症状
* JsonItem[] patterns: 主治证型
* JsonItem[] treatments: 相关治则
* JsonItem[] westernDiagnoses: 西医病名
* JsonItem usage: 用法
* JsonClassicFormulaHerb[] herbs: 用药
* JsonClassicFormulaMod[] mods: 加减味
* String remark: 备注

_JsonClassicFormulaModHerb_

* Long id: 中药编号
* String name: 名称
* BigDecimal minCount: 最大用量
* BigDecimal maxCount: 最小用量
* String unit: 单位

_JsonClassicFormulaMod_

* Long id: 加减味编号
* String source: 来源出处
* JsonClassicFormulaModHerb[] herbsAddition: 加味药
* JsonItem[] herbsRemoval: 减味药
* JsonItem[] symptoms: 相关症状
* JsonItem[] patterns: 相关证型

_JsonItem_

* Long id: 编号
* String name: 名称

### getHerbInfo (取得中草药资料) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	String name: 中草药名称
	
output:

	JsonResult<JsonExpHerbInfo>: 中草药资料，若无法辨识药名，则回传名称为空字串的回传值
	
_JsonExpHerbInfo_

* String name: 药名
* String[] xing: 药性
* String[] wei: 药味
* String[] channels: 归经
* String[] categories: 分类
* String[] standardUsage: 标准用法
* String[] poison: 毒麻饮片
* String[] symptoms: 主治症状
* String[] patterns: 主治证型
* String[] treatments: 相关治则
* String[] forbidSymptoms: 禁忌症状
* String[] forbidPatterns: 禁忌证型

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### getClassicFormulaList (取得经典方完整列表) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer page: 页数，从 0 开始，抓到回传空阵列代表最后一页

output:

	JsonClassicFormula[]: 经典方列表

_JsonItem_

* Long id: 编号
* String name: 名称
	
_JsonClassicFormulaHerb_

* Integer id: 经典方中药编号
* String name: 中药名称
* BigDecimal dose: 剂量
* String doseUnit: 单位

_JsonClassicFormulaModHerb_

* Long id: 中药编号
* String name: 名称
* BigDecimal minCount: 最大用量
* BigDecimal maxCount: 最小用量
* String unit: 单位

_JsonClassicFormulaMod_

* Long id: 加减味编号
* String source: 来源出处
* JsonClassicFormulaModHerb[] herbsAddition: 加味药
* JsonItem[] herbsRemoval: 减味药
* JsonItem[] symptoms: 相关症状
* JsonItem[] patterns: 相关证型

_JsonClassicFormula_

* Long id: 经典方编号
* String name: 名称
* JsonItem[] categories: 分类
* String source: 出处
* JsonItem[] symptoms: 主治症状
* JsonItem[] patterns: 主治证型
* JsonItem[] treatments: 相关治则
* JsonItem[] westernDiagnoses: 西医病名
* JsonItem usage: 用法
* JsonClassicFormulaHerb[] herbs: 用药
* JsonClassicFormulaMod[] mods: 加减味
* String remark: 备注

### fetchClassicFormulasWithCategory (取得经典方分类目录列表) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	Integer page: 页数，从 0 开始，抓到回传空阵列代表最后一页

output:

	JsonClassicFormulaCategory[]: 经典方分类目录列表
	
_JsonClassicFormulaListItem_

* Long id: 经典方编号
* String name: 经典方名称
* JsonItem[] patterns: 主治证型

_JsonClassicFormulaCategory_

* Long id: 分类编号
* String name: 分类名称
* JsonClassicFormulaCategory[] children: 子分类
* JsonClassicFormulaListItem[] formulas: 内含经典方


