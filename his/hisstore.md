# 徒帝严选商城功能 (v.0.9) #

徒帝严选商城功能是一个介面模块，让服务供应商可以在系统上镶入徒帝严选商城，这个严选商城能够让诊所成员在上面选购徒帝严选的保健品、保养品、保健服务，当诊所成功推荐商品给使用者，将寄送资料填妥，系统会产称付款二维码让使用者直接扫码付款，付款成功后商品将直接寄送至使用者指定地点。镶入介面如下：

![协定方目录介面](asset/hisstore.png)

## 版本变更 ##

### v.0.9 ###

* 加入徒帝严选商城功能

## 接入说明 ##

接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>The HIS Store Widget Demo</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1200,
    height: 800,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'hisstore',
    token: '诊所成员 token'
	};
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

1. 需要引入 widget.js 的 javascript library "https://module.tudihis.cn/widget/widget.js"
2. 准备好 config 资料
	* width, height: 单位是 px，为模块镶入时设置的大小
	* domain: 请传入**镶入模块的主页面域名或网址**，模块要传送信息给主页面，必须得先指定域名网址，主页面才能顺利接收到信息
	* callback: 目前不会 callback，但请不要留空，可以传入不做事的 function，例如 () => {}
	* element: 指定模块镶入的 HTML element
	* type: 请填入 'hisstore'
	* token: 请传入**诊所成员 token**
3. 使用 cn.tudihis.widget.init(config) 就可以把模块镶入了
4. 若因为内网 nginx 转址等需求，造成连入的模块网址不是原本的 https://module.tuditcm.com ，则需要修改两个地方：
	* javascript library 引入的网址
	* 需要使用 cn.tudihis.widget.setUrl 指定新的网址
