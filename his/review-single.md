# 单次处方点评功能 (v.0.9.6) #

单次处方点评提供 HIS 有能力将医生的处方送至中医大脑，产生点评报告书。镶入介面如下：

![单次处方点评介面](asset/reviewsingle.png)

## 版本变更 ##

### v.0.9.7 ###

* reviewSingleSimpleRecord 回传值加入 oriClinicId

### v.0.9.6 ###

* getReasonableReport 回传值加入 score

### v.0.9.5 ###

* 加入 getReasonableReport

### v.0.9.4 ###

* 加入 reviewSingleSimpleRecord

### v.0.9.3 ###

* addSingleRecordReview 传出值改为 JsonResult

### v.0.9.2 ###

* JsonExternalInputRecord 新增栏位 prescriptions、oriClinicId
* JsonExternalInputRecord 删除栏位 prescription、formulaCount
* 新增 JsonExternalInputPrescription

### v.0.9.1 ###

* JsonExternalInputRecord 新增栏位 oriDoctorId
* JsonExternalInputPatient 栏位 birthday 改为 String，格式 yyyy/MM/dd
* JsonExternalInputRecord 栏位 recordTime 改为 String，格式 yyyy/MM/dd HH:mm:ss

### v.0.9 ###

* 加入单次处方点评功能

## 操作流程 ##

* Step 1: 当医生启动单次处方点评功能，请调用 API addSingleRecordReview，将医生的处方与病人基本信息传给 Tudi 系统，系统会回传一组点评 token。
* Step 2: 使用介面模块 review-single，config 中带入点评 token，就可以在指定的 element 中画出点评报告书。

## API ##

调用 API 网址为

	https://module.tudihis.cn/cnmodule/kbs/[operation]

若需要权限验证，则需要在 http header 加入

	Authorization = [token]
	
Input 参数使用 post 传送

---

### addSingleRecordReview (加入被点评的单张处方) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputSingleReview: 处方病人资料，请用 post body 传入
	
output:

	JsonResult<String>: 点评 token
	
_JsonExternalInputClinic_

* Long oriId: 原系统诊所编号
* String name: 原系统诊所名称

_JsonExternalInputPatient_

* Long oriId: 原系统病人编号
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* String birthday: 生日，yyyy/MM/dd
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputRecord_

* Long oriId: 原系统就诊记录编号
* String chiefComplaint: 主诉
* String currentHistory: 现病史
* String patternTreatment: 证型治则
* JsonExpRecordDiagnosis[] diagnosis: 诊断
* JsonExternalInputPrescription[] prescriptions: 处方
* String recordTime: 就诊时间，yyyy/MM/dd HH:mm:ss
* Long oriPatientId: 原系统病人编号
* Long oriDoctorId: 原系统医生编号，单次处方点评放 0
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputPrescription_

* Long oriPrescriptionId: 原系统处方编号
* Integer formulaCount: 帖数
* String usage: 用法，例如口服、代茶饮，用来区分不同处方用法不同
* JsonExpPrescriptionHerb[] prescription: 处方

_JsonExternalInputSingleReview_

* JsonExternalInputClinic clinic: 诊所资料
* JsonExternalInputPatient patient: 病人资料
* JsonExternalInputRecord record: 就诊记录资料

_JsonExpPrescriptionHerb_

* Long id: 请带入 0 即可
* String name: 中药名称
* BigDecimal dose: 单帖剂量
* String unit: 单位
* BigDecimal price: 单价

_JsonExpRecordDiagnosis_

* String insId: 诊断编码，若无编码请传空字串
* String name: 诊断名称

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### reviewSingleSimpleRecord (简单点评的单张处方) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputSingleReview: 处方病人资料，请用 post body 传入
	
output:

	JsonResult<JsonExpSimpleRecord>: 点评结果
	
_JsonExternalInputClinic_

* Long oriId: 原系统诊所编号
* String name: 原系统诊所名称

_JsonExternalInputPatient_

* Long oriId: 原系统病人编号
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* String birthday: 生日，yyyy/MM/dd
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputRecord_

* Long oriId: 原系统就诊记录编号
* String chiefComplaint: 主诉
* String currentHistory: 现病史
* String patternTreatment: 证型治则
* JsonExpRecordDiagnosis[] diagnosis: 诊断
* JsonExternalInputPrescription[] prescriptions: 处方
* String recordTime: 就诊时间，yyyy/MM/dd HH:mm:ss
* Long oriPatientId: 原系统病人编号
* Long oriDoctorId: 原系统医生编号，单次处方点评放 0
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputPrescription_

* Long oriPrescriptionId: 原系统处方编号
* Integer formulaCount: 帖数
* String usage: 用法，例如口服、代茶饮，用来区分不同处方用法不同
* JsonExpPrescriptionHerb[] prescription: 处方

_JsonExternalInputSingleReview_

* JsonExternalInputClinic clinic: 诊所资料
* JsonExternalInputPatient patient: 病人资料
* JsonExternalInputRecord record: 就诊记录资料

_JsonExpPrescriptionHerb_

* Long id: 请带入 0 即可
* String name: 中药名称
* BigDecimal dose: 单帖剂量
* String unit: 单位
* BigDecimal price: 单价

_JsonExpRecordDiagnosis_

* String insId: 诊断编码，若无编码请传空字串
* String name: 诊断名称

_JsonExpSimpleRecord_

* Long id: 就诊记录编号，为 0
* Long oriId: 原系统就诊记录编号
* Long oriClinicId: 原系统医疗机构编号
* JsonExpPatientInfo patientInfo: 病人资料
* String[] diagnoses: 诊断
* Integer[] violationIds: 违反规则编号
	* 1 = 记录症狀 (扣 10 分)
	* 2 = 记录舌象 (扣 10 分)
	* 3 = 记录脉象 (扣 10 分)
	* 4 = 记录证型 (扣 10 分)
	* 5 = 记录诊断病名 (扣 10 分)
	* 6 = 记录诊断证型 (扣 10 分)
	* 7 = 不违反十八反十九畏 (扣 70 分)
	* 8 = 不违反安全用药 (扣 90 分)
	* 9 = 不违反普通疾病不超过20味 (扣 20 分)
	* 10 = 不违反肿瘤不超过25味 (扣 20 分)
	* 11 = 不违反普通病不超过7贴/次 (扣 20 分)
	* 12 = 不违反慢性病不超过14贴/次 (扣 20 分)
	* 15 = 不连续使用大毒中药 (扣 90 分)
	* 16 = 不对老人使用危险中药 (扣 90 分)
	* 17 = 不对儿童使用危险中药 (扣 90 分)
	* 18 = 不对孕妇使用危险中药 (扣 90 分)
	* 19 = 症状证型符合 (扣 20 分)
	* 21 = 方证符合 (扣 20 分)
	* 22 = 符合常规用药量 (扣 10 分)
	* 23 = 大毒饮片不超量 (扣 90 分)
	* 24 = 贵细饮片不超量 (扣 70 分)
	* 25 = 超过半数饮片不超量 (扣 70 分)
* Integer score: 得分 0 - 100
* Integer warningLevel: 警告等级
	* 0 = 没问题 (score >= 80)
	* 1 = 打招乎 (score >= 60 && score < 80)
	* 2 = 提醒 (score >= 40 && score < 60)
	* 3 = 警告 (score >= 20 && score < 40)
	* 4 = 禁止 (score < 20)
* LocalDateTime finishTime: 就诊时间

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

### getReasonableReport (取得合理用药报告) ###

所需权限：诊所成员 token 或服务供应商 token

input:

	JsonExternalInputSingleReview: 处方病人资料，请用 post body 传入
	
output:

	JsonResult<JsonExternalReasonableReport>: 合理用药报告

_JsonExternalInputClinic_

* Long oriId: 原系统诊所编号
* String name: 原系统诊所名称

_JsonExternalInputPatient_

* Long oriId: 原系统病人编号
* Integer genderId: 性别，1 = 男, 2 = 女, 3 = 未说明性别
* String birthday: 生日，yyyy/MM/dd
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputRecord_

* Long oriId: 原系统就诊记录编号
* String chiefComplaint: 主诉
* String currentHistory: 现病史
* String patternTreatment: 证型治则
* JsonExpRecordDiagnosis[] diagnosis: 诊断
* JsonExternalInputPrescription[] prescriptions: 处方
* String recordTime: 就诊时间，yyyy/MM/dd HH:mm:ss
* Long oriPatientId: 原系统病人编号
* Long oriDoctorId: 原系统医生编号，单次处方点评放 0
* Long oriClinicId: 原系统诊所编号

_JsonExternalInputPrescription_

* Long oriPrescriptionId: 原系统处方编号
* Integer formulaCount: 帖数
* String usage: 用法，例如口服、代茶饮，用来区分不同处方用法不同
* JsonExpPrescriptionHerb[] prescription: 处方

_JsonExternalInputSingleReview_

* JsonExternalInputClinic clinic: 诊所资料
* JsonExternalInputPatient patient: 病人资料
* JsonExternalInputRecord record: 就诊记录资料

_JsonExpPrescriptionHerb_

* Long id: 请带入 0 即可
* String name: 中药名称
* BigDecimal dose: 单帖剂量
* String unit: 单位
* BigDecimal price: 单价

_JsonExpRecordDiagnosis_

* String insId: 诊断编码，若无编码请传空字串
* String name: 诊断名称

_JsonExternalReasonableItem_

* Integer type: 警告类型
	* 1 = 十八反十九畏
	* 2 = 经典方禁忌
	* 3 = 用药禁忌
	* 4 = 大毒饮片超量
	* 5 = 老幼孕妇禁忌
* String message: 讯息

_JsonExternalReasonableReport_

* JsonExternalReasonableItem[] warningList: 警告列表
* JsonExternalReasonableItem[] dangerList: 危险列表
* Integer score: 0 - 100 分

_JsonResult<T>_

* T data: 正常状态传出值，格式为 T 的类别
* Integer errorCode: 错误编号，0 代表正常
* String errorMessage: 错误信息

## 介面模块接入 ##

接入范例程式如下：

```
<html>
<head>
  <script type="text/javascript" src="https://module.tudihis.cn/widget/widget.js"></script>
</head>
<body>
  <h1>Review Single</h1>
  <div id="target"></div>
<script>
  let cb = (data) => {
    console.log('get callback', data);
  };
  const config = {
    width: 1000,
    height: 600,
    domain: encodeURIComponent('https://serviceprovider.demo.site'),
    callback: cb,
    element: document.getElementById('target'),
    type: 'review-single',
    token: '诊所成员 token'
		kbsToken: 'addSingleRecordReview 回传的点评 token'
	};
  cn.tudihis.widget.setUrl('https://module.new.site'); // 若使用预设网址则不需此行
  cn.tudihis.widget.init(config);
</script>
</body>
</html>
```

1. 需要引入 widget.js 的 javascript library "https://module.tudihis.cn/widget/widget.js"
2. 准备好 config 资料
	* width, height: 单位是 px，为模块镶入时设置的大小
	* domain: 请传入**镶入模块的主页面域名或网址**，模块要传送信息给主页面，必须得先指定域名网址，主页面才能顺利接收到信息
	* callback: 此模块无回传动作
	* element: 指定模块镶入的 HTML element
	* type: 请填入 'review-single'
	* token: 请传入**诊所成员 token** 或 **服务供应商 token**
	* kbsToken: 请传入第一步 API 回传的点评 token
3. 使用 cn.tudihis.widget.init(config) 就可以把模块镶入了
4. 若因为内网 nginx 转址等需求，造成连入的模块网址不是原本的 https://module.tuditcm.com ，则需要修改两个地方：
	* javascript library 引入的网址
	* 需要使用 cn.tudihis.widget.setUrl 指定新的网址

