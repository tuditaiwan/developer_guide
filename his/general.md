# 基础管理功能 (v.0.9) #

基础管理功能为 API 形式的管理功能，让服务供应商登入取得**服务供应商 token** (login)，取得 token 后，可以以此权限建立诊所列表 (registerClinics)、更新诊所成员列表 (updateClinicMembers, updateClinicMembersAndRemoveOther)，当诊所成员需要使用模块其他加值功能时，需要帮诊所成员签入 (signInClinic)，并取得**诊所成员 token**。

## 版本变更 ##

### v.0.9 ###

* 提供服务供应商登入功能
* 提供诊所管理与签入功能

## API ##

调用 API 网址为

	https://module.tudihis.cn/cnmodule/general/[operation]

若需要权限验证，则需要在 http header 加入

	Authorization = [token]

Input 参数使用 post 传送

---

### login (服务供应商登入) ###

所需权限：不需要 token

input:

	String account: 账号
	String password: 密码

output:

	JsonLogin: 登入信息，其中包含服务供应商 token，每次登入皆会取得相同的 服务供应商 token，且不会失效，因此不需要一直登入

_JsonLogin_

* CurrentAccount currentAccount: 目前登入信息
* String token: 服务供应商 token

_CurrentAccount_

* Integer memberId: 成员编号
* String account: 成员账号
* String name: 成员名称
* Integer clinicId: 诊所编号，在此处为 NULL
* String clinicName: 诊所名称，在此处为 NULL
* Integer serviceId: 服务供应商编号
* String serviceName: 服务供应商名称
* Integer tokenType: token 类型，应为 4，服务供应商
* String timeToken: 登入時間戳記

---

### registerClinics (注册诊所) ###

所需权限：服务供应商 token

input:

	JsonClinic[]: 诊所列表，写在 request body 中，clinicCode 可为空字串，会自动产生，其他栏位必填，诊所会以 name 为辨认依据，因此系统不允许注册同样名称的诊所

output:

	JsonClinic[]: 已建立的诊所列表

_JsonClinic_

* String name: 诊所名称
* String clinicCode: 诊所代码
* String owner: 诊所负责人姓名
* String address: 诊所地址
* String phone: 诊所电话

---

### updateClinic (修改诊所资料) ###

所需权限：服务供应商 token 或诊所成员 token

input:

	JsonClinic: 欲修改的诊所资料，写在 request body 中，注意，是否为相同诊所是以 name 来区分，所以不能更改，若为诊所成员 token，只能编辑所属的诊所

output:

	JsonClinic: 修改完毕的诊所资料

_JsonClinic_

* String name: 诊所名称
* String clinicCode: 诊所代码
* String owner: 诊所负责人姓名
* String address: 诊所地址
* String phone: 诊所电话

---

### updateClinicMembers (修改诊所成员) ###

所需权限：服务供应商 token 或诊所成员 token

input:

	JsonClinicMemberList: 诊所成员列表，写在 request body 中，其中列表中的 account 若已在模块系统中，则为修改，若不存在，则为新增，若模块系统中有这次传入列表中未出现的 account，则不做任何修改

output:

	JsonClinicMember[]: 修改完毕的诊所成员资料

_JsonClinicMemberList_

* String clinicName: 诊所名称，此名称必须是已注册的诊所
* JsonClinicMember[] members: 成员列表

_JsonClinicMember_

* String account: 成员账号
* String name: 成员姓名
* String phone: 成员电话

---

### updateClinicMembersAndRemoveOther (修改诊所成员并删除没传入的账号) ###

所需权限：服务供应商 token 或诊所成员 token

input:

	JsonClinicMemberList: 诊所成员列表，写在 request body 中，其中列表中的 account 若已在模块系统中，则为修改，若不存在，则为新增，若模块系统中有这次传入列表中未出现的 account，则删除，这个 API 可以看作是将诊所成员 replace 成传入的列表

output:

	JsonClinicMember[]: 修改完毕的诊所成员资料

_JsonClinicMemberList_

* String clinicName: 诊所名称，此名称必须是已注册的诊所
* JsonClinicMember[] members: 成员列表

_JsonClinicMember_

* String account: 成员账号
* String name: 成员姓名
* String phone: 成员电话

---

### signInClinic (服务供应商帮诊所成员签入) ###

所需权限：服务供应商 token

input:

	Integer clinicId: 诊所编号，若无则传 0
	String clinicName: 诊所名称，若无则传空字串，clinicId, clinicName 必须选择传入一项
	String account: 签入的诊所成员账号

output:

	JsonLogin: 诊所成员登入信息，其中包含诊所成员 token，请特别注意，取得的诊所成员 token 若 24 小时没有使用则失效，若同一个诊所成员账号再次签入，则之前的 token 将会全部失效

_JsonLogin_

* CurrentAccount currentAccount: 目前登入信息
* String token: 诊所成员 token

_CurrentAccount_

* Integer memberId: 诊所成员编号
* String account: 诊所成员账号
* String name: 诊所成员名称
* Integer clinicId: 诊所编号
* String clinicName: 诊所名称
* Integer serviceId: 服务供应商编号
* String serviceName: 服务供应商名称
* Integer tokenType: token 类型，应为 5，诊所成员
* String timeToken: 登入時間戳記